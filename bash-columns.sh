#!/usr/bin/env bash

# (1) file 'a' 10 cols, tab separated, 10 rows
# GLOBAL VARIABLES
SCRIPT_DIR="${PWD}/input/create"

# (1 - d)
function initialize
{
    if [ ! -d ${SCRIPT_DIR} ]; then
        mkdir -vp $PWD/input/create
    fi
}

# (1 - a,b,c)
function gen_col
{
    LINE_NO=$(seq 1 10)
    pushd ${SCRIPT_DIR}

    for i in $LINE_NO
    do
    RANDOM_NUM=$(shuf -i 0-100000 -n 8)
    RANDOM_STRING=$(head /dev/urandom | tr -dc A-Za-z0-9 | head -c 6)
        echo -e ${i}"\t"${RANDOM_STRING}"\t"${RANDOM_NUM}
    done > a
    popd
}

function teardown
{
    if [ -d ${SCRIPT_DIR} ]; then
        rm -rfv ${SCRIPT_DIR}
    fi
}

# (2)
function redir_col
{
    awk '{print $8, $9}' ${SCRIPT_DIR}/a > ${SCRIPT_DIR}/b
}

# (3)
function sum_col
{
    awk '{sum += $1} END {print sum}' ${SCRIPT_DIR}/b > ${SCRIPT_DIR}/c
}

# (4)
function str_replace
{
    sed --expression 's/1/0/g' ${SCRIPT_DIR}/b > ${SCRIPT_DIR}/d
}

initialize
gen_col
redir_col
sum_col
str_replace
#teardown