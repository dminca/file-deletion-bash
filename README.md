We discovered data on a NFS share that contains millions of images wich are stored in a directory tree that looks
like the following.

`/data/<country>/<year in form YYYY>/<directory>/<directory>/.../*.jpg`

The NFS share is also used by other processes that put new data on the system (in 2018 directories) and read files.
There are about 30 country directories present.

The year directories range from 2009 to 2018.

We decided that we only need the data for the last 24 months.
1. Please create a script that deletes all data stored in the years directory for the years 2009 to 2015.
2. Please make a rough assumption on the runtime of your script (rough estimate on RAID 6 filer), if
the script deletes 1M pictures with each 1 MB size.
3. Please create a script that deletes all files and directories, if it resides under a year directory that is
not the current year and the two years before that (so currently, it would delete all data in the
directories from 2009 to 2015).
4. Please create a script that deletes all data that is older than 24 months in that file structure.
