#!/bin/bash

# (1) delete data between 2009-2015
find /data -type d -name '20*' \
    -not -path '/data/*/2016/*' \
    -or -not -path '/data/*/2017/*' \
    -or -not -path '/data/*/2018/*' \
    -exec rm -rfv {} \;

# (3) delete 2009-2015
find /data -type d -name '20*'
    -not -path '/data/*/2016/*' \
    -or -not -path '/data/*/2017/*' \
    -or -not -path "/data/*/$(date +%G)/*" \
    -exec rm -rfv {} \;

# (4) remove data older than 24 months
# touch my timestamp as a file
touch -t 201601010000 /tmp/2016-Jan-01-0000

# use the timestamp file as a reference point
find /data/ -type d ! -newer /tmp/2016-Jan-01-0000 | xargs rm -rfv